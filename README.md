# Bem vindo ao Projeto Usuários
Ao clonar o projeto, abra as pastas `frontend` e `backend` em IDEs/editores diferentes (a pasta backend de preferência no IntelliJ).

## FrontEnd
Instalação Node.js (caso não tenha):
https://nodejs.org/en/

Após a instalação do Node.js use o npm para instalação global do vue.js:

    npm install -g vue

Instalação do client do Vue:

    npm install -g @vue/cli

Para subir a aplicação em localhost:8081.

    npm install
    npm run dev

Caso esteja faltando a dependencia webpack-dev-server, instale usando este comando:

    npm install -g webpack-dev-server@1
    npm run dev

## Backend
Após abrir a pasta `Backend` no IntelliJ, e configurar o Java no projeto, o Maven irá instalar as dependências.

Crie o schema com nome `usuarios` no Mysql:

    CREATE SCHEMA `usuarios` ;

Altere suas configurações da conexão do Mysql no arquivo:

    ..\backend\src\main\resources\application.properties

Execute a aplicação `FundeccUsuariosApplication` para subir em localhost:8080.