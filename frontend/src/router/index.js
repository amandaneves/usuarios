import Vue from 'vue'
import Router from 'vue-router'
import Inicio from '@/components/Inicio'
import Cargos from '@/components/Cargos'
import Perfis from '@/components/Perfis'
import Usuarios from '@/components/Usuarios'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Inicio',
      component: Inicio
    },
    {
      path: '/cargos',
      name: 'Cargos',
      component: Cargos
    },
    {
      path: '/perfis',
      name: 'Perfis',
      component: Perfis
    },
    {
      path: '/usuarios',
      name: 'Usuarios',
      component: Usuarios
    }
  ]
})
