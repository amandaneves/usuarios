package com.fundecc.usuarios.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fundecc.usuarios.enums.Genero;

import javax.persistence.*;
import java.time.LocalDate;

@MappedSuperclass
public abstract class Pessoa extends DataCadastro {

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false, unique = true)
    private String cpf;

    @Column(nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dataNascimento;

    @Column(nullable = false, length = 1)
    private String sexo;

    @Transient
    private Genero valorGenero;

    @PostLoad
    void fillTransient() {
        if (sexo != null) {
            this.valorGenero = Genero.of(sexo);
        }
    }

    @PrePersist
    void fillPersistent() {
        if (valorGenero != null) {
            this.sexo = valorGenero.getLetra();
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
}
