package com.fundecc.usuarios.repositories;

import com.fundecc.usuarios.models.Perfil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PerfilRepository extends JpaRepository<Perfil, Long> {

    @Query(value = "SELECT * FROM perfis WHERE nome LIKE ?1", nativeQuery = true)
    public Perfil buscarPorNome(String nome);

}
