package com.fundecc.usuarios.repositories;

import com.fundecc.usuarios.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    @Query(value = "SELECT * FROM usuarios WHERE cpf LIKE ?1", nativeQuery = true)
    public Usuario buscarPorCpf(String cpf);

}
