package com.fundecc.usuarios.repositories;

import com.fundecc.usuarios.models.Cargo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CargoRepository extends JpaRepository<Cargo, Long> {

    @Query(value = "SELECT * FROM cargos WHERE nome LIKE ?1", nativeQuery = true)
    public Cargo buscarPorNome(String nome);

}
