package com.fundecc.usuarios.controllers;

import com.fundecc.usuarios.models.Usuario;
import com.fundecc.usuarios.services.UsuarioServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/api/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioServiceInterface usuarioService;

    @GetMapping("")
    public List<Usuario> listar() throws Exception {
        return this.usuarioService.listar();
    }

    @GetMapping("/{id}")
    public Optional<Usuario> buscar(@PathVariable("id") Long id) throws Exception {
        return this.usuarioService.buscar(id);
    }

    @PostMapping("")
    public Usuario incluir(@RequestBody Usuario entity) throws Exception {
        return this.usuarioService.incluir(entity);
    }

    @PutMapping("")
    public Usuario alterar(@RequestBody Usuario entity) throws Exception {
        return this.usuarioService.alterar(entity);
    }

    @DeleteMapping("/{id}")
    public void excluir(@PathVariable("id") Long id) throws Exception {
        this.usuarioService.excluir(id);
    }
}
