package com.fundecc.usuarios.controllers;

import com.fundecc.usuarios.models.Cargo;
import com.fundecc.usuarios.services.CargoServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/api/cargos")
public class CargoController {

    @Autowired
    private CargoServiceInterface cargoService;

    @GetMapping("")
    public List<Cargo> listar() throws Exception {
        return this.cargoService.listar();
    }

    @GetMapping("/{id}")
    public Optional<Cargo> buscar(@PathVariable("id") Long id) throws Exception {
        return this.cargoService.buscar(id);
    }

    @PostMapping("")
    public Cargo incluir(@RequestBody Cargo entity) throws Exception {
        return this.cargoService.incluir(entity);
    }

    @PutMapping("")
    public Cargo alterar(@RequestBody Cargo entity) throws Exception {
        return this.cargoService.alterar(entity);
    }

    @DeleteMapping("/{id}")
    public void excluir(@PathVariable("id") Long id) throws Exception {
        this.cargoService.excluir(id);
    }
}
