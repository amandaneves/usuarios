package com.fundecc.usuarios.controllers;

import com.fundecc.usuarios.models.Perfil;
import com.fundecc.usuarios.services.PerfilServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/api/perfis")
public class PerfilController {

    @Autowired
    private PerfilServiceInterface perfilService;

    @GetMapping("")
    public List<Perfil> listar() throws Exception {
        return this.perfilService.listar();
    }

    @GetMapping("/{id}")
    public Optional<Perfil> buscar(@PathVariable("id") Long id) throws Exception {
        return this.perfilService.buscar(id);
    }

    @PostMapping("")
    public Perfil incluir(@RequestBody Perfil entity) throws Exception {
        return this.perfilService.incluir(entity);
    }

    @PutMapping("")
    public Perfil alterar(@RequestBody Perfil entity) throws Exception {
        return this.perfilService.alterar(entity);
    }

    @DeleteMapping("/{id}")
    public void excluir(@PathVariable("id") Long id) throws Exception {
        this.perfilService.excluir(id);
    }
}
