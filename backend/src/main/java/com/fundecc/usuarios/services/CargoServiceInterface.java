package com.fundecc.usuarios.services;

import com.fundecc.usuarios.models.Cargo;
import com.fundecc.usuarios.models.Usuario;

import java.util.List;
import java.util.Optional;

public interface CargoServiceInterface {

    List<Cargo> listar() throws Exception;

    Optional<Cargo> buscar(Long id) throws Exception;

    Cargo incluir(Cargo entity) throws Exception;

    Cargo alterar(Cargo entity) throws Exception;

    void excluir(Long id) throws Exception;

}
