package com.fundecc.usuarios.services;

import com.fundecc.usuarios.models.Perfil;
import com.fundecc.usuarios.repositories.PerfilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PerfilService implements PerfilServiceInterface {

    @Autowired
    PerfilRepository perfilRepository;

    @Override
    public List<Perfil> listar() {
        return this.perfilRepository.findAll();
    }

    @Override
    public Optional<Perfil> buscar(Long id) {
        return this.perfilRepository.findById(id);
    }

    @Override
    public Perfil incluir(Perfil entity) {
        Perfil temPerfilComMesmoNome = this.perfilRepository.buscarPorNome(entity.getNome());

        if (temPerfilComMesmoNome != null) {
            return null;
        } else {
            return this.perfilRepository.save(entity);
        }
    }

    @Override
    public Perfil alterar(Perfil entity) {
        Boolean existePerfil = this.perfilRepository.existsById(entity.getId());

        if (existePerfil) {
            return this.perfilRepository.save(entity);
        } else {
            return null;
        }
    }

    @Override
    public void excluir(Long id) {
        this.perfilRepository.deleteById(id);
    }
}
