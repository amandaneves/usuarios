package com.fundecc.usuarios.services;

import com.fundecc.usuarios.models.Cargo;
import com.fundecc.usuarios.repositories.CargoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CargoService implements CargoServiceInterface {

    @Autowired
    CargoRepository cargoRepository;

    @Override
    public List<Cargo> listar() {
        return this.cargoRepository.findAll();
    }

    @Override
    public Optional<Cargo> buscar(Long id) {
        return this.cargoRepository.findById(id);
    }

    @Override
    public Cargo incluir(Cargo entity) {
        Cargo temCargoMesmoNome = this.cargoRepository.buscarPorNome(entity.getNome());

        if (temCargoMesmoNome != null) {
            return null;
        } else {
            return this.cargoRepository.save(entity);
        }
    }

    @Override
    public Cargo alterar(Cargo entity) {
        Boolean existeCargo = this.cargoRepository.existsById(entity.getId());

        if (existeCargo) {
            return this.cargoRepository.save(entity);
        } else {
            return null;
        }
    }

    @Override
    public void excluir(Long id) {
        this.cargoRepository.deleteById(id);
    }
}
