package com.fundecc.usuarios.services;

import com.fundecc.usuarios.models.Usuario;
import com.fundecc.usuarios.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService implements UsuarioServiceInterface {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Override
    public List<Usuario> listar() {
        return usuarioRepository.findAll();
    }

    @Override
    public Optional<Usuario> buscar(Long id) {
        return this.usuarioRepository.findById(id);
    }

    @Override
    public Usuario incluir(Usuario entity) {
        Usuario temUsuarioMesmoCpf = usuarioRepository.buscarPorCpf(entity.getCpf());

        if (temUsuarioMesmoCpf != null) {
            return null;
        } else {
            return this.usuarioRepository.save(entity);
        }
    }

    @Override
    public Usuario alterar(Usuario entity) {
        Boolean existeUsuario = this.usuarioRepository.existsById(entity.getId());

        if (existeUsuario) {
            return this.usuarioRepository.save(entity);
        } else {
            return null;
        }
    }

    @Override
    public void excluir(Long id) {
        this.usuarioRepository.deleteById(id);
    }

}
