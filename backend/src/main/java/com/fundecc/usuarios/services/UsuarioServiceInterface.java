package com.fundecc.usuarios.services;

import com.fundecc.usuarios.models.Usuario;

import java.util.List;
import java.util.Optional;

public interface UsuarioServiceInterface {

    List<Usuario> listar() throws Exception;

    Optional<Usuario> buscar(Long id) throws Exception;

    Usuario incluir(Usuario entity) throws Exception;

    Usuario alterar(Usuario entity) throws Exception;

    void excluir(Long id) throws Exception;

}
