package com.fundecc.usuarios.services;

import com.fundecc.usuarios.models.Perfil;

import java.util.List;
import java.util.Optional;

public interface PerfilServiceInterface {

    List<Perfil> listar() throws Exception;

    Optional<Perfil> buscar(Long id) throws Exception;

    Perfil incluir(Perfil entity) throws Exception;

    Perfil alterar(Perfil entity) throws Exception;

    void excluir(Long id) throws Exception;
}
